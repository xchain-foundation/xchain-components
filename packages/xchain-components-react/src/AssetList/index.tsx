import React from 'react';

export interface AssetListProps {
  className?: string;
}

const AssetList: React.FC<AssetListProps> = ({ className = '' }) => (
  <select className={className}></select>
);

export default AssetList;
