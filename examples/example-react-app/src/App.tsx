import { AssetList } from '@xchain-foundation/xchain-components-react';

function App() {
  return (
    <div className="App">
      <AssetList />
    </div>
  );
}

export default App;
